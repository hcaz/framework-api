api_framework
=============

This is a simple API framework written in PHP  
This works by uploading both api.php and functions.php to the same folder on your server and then sending POST or get requests to api.php (This can be renamed index.php).  
The system can be changed by modifying functions.php with all the methods needed and updating the methods array for security checks. The framework has some build in commands that are listed below aswell as it handling inputs and outputs checking for required feilds.  
  
Extra's  
?error=true - This sets php internal error checking to true for debugging.  
?format=(json|xml|print) - This changed the encoded output to different markup languages, this defualts to json.  
?callback=(null|false|URL) - This is weather or not HTTP codes are sent, with false they are not, with null they are, with a URL in there it 302 rediects to that url with the data encoded in the URL.  
?help=true - This displays the array for that method with all the inputs and details.

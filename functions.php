<?php
  /***************************************
  * hcaz API Framework.                  *
  * Made in 2014, version 1.0            *
  * Do whatever you want with this.      *
  ***************************************/
  $methods['gettingStarted'] = array(
    'about'=>'This is the getting started function for you to copy.',
    'inputs'=>array(
      array('name'=>'name','required'=>true,'about'=>'This is your first name only.'),
      array('name'=>'url','required'=>false,'about'=>'URL where this api is sitting.')
    )
  );
  function gettingStarted($DATA){
    //Take input from $DATA, it contains both $_GET and $_POST, so use $DATA and its all ok.
    $name = $DATA['name'];
    //Build up data
    $welcomeMessage = 'Hello '.$name.', welcome to your new API framework.';
    $howToMessage = 'In the functions.php file you may copy the gettingStarted function to make your own, first thing is you need to build up there array first with the name of the function and information about the function, you can add an about message and all the inputs. If an input is set to required then the API framework will handle it for you and make sure the user provides it for you. Then you use the array provided called $DATA which contains all the inputs from the user, when ready to return to the user you can just return array(); which everything you need, the API framework will convert that to json, xml or php print based on if the user passes `format` which can be either `xml` or `print` as it defaults to `json`. Users can also specify `callback`, this can be used so that http codes will not be set if callback=false is passed or if callback=http://google.com then the API will redirect to that with a base64 encoded version of the data for forms and stuff. When returning if you do not specify a http code it will be 200, you can specify one by giving CODE in the array like return array(\'CODE\'=>404);';
    $helpMessage = 'One more thing, if at any time you pass a method name and help=true it will show you what is in the array for that method like what inputs there are.';
    $thanksMessage = 'Thanks for using our framework :)';
    //Return response
    return array('CODE'=>200,'messages'=>array('welcomeMessage'=>$welcomeMessage,'howToMessage'=>$howToMessage,'helpMessage'=>$helpMessage,'thanksMessage'=>$thanksMessage));
  }
?>

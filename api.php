<?php
  /***************************************
  * hcaz API Framework.                  *
  * Made in 2014, version 1.0            *
  * Do whatever you want with this.      *
  ***************************************/
  //Testing
  //Testing 2
  /* Get input */
  if(api_framework_isblank($_REQUEST['method'])){$method = null;}else{$method = $_REQUEST['method'];}
  if(api_framework_isblank($_REQUEST['callback'])){$callback = null;}else{$callback = $_REQUEST['callback'];}
  if(api_framework_isblank($_REQUEST['format'])){$format = null;}else{$format = $_REQUEST['format'];}
  if(!api_framework_isblank($_REQUEST['format'])){
    if($_REQUEST['error']=='true'){
      error_reporting(E_ALL);
      ini_set('display_errors', 1);
    }else{
      ini_set('display_errors', 0);
    }    
  }
  /* Load functions */
  require("functions.php");
  /* Run method */
  if($method==null){
    api_framework_output(400, array('errors'=>'No method passed','fix'=>'Pass `method` via POST or GET'));
  }else{
    if(api_framework_isblank($methods[$method])){
      api_framework_output(404, array('error'=>'Method not recognized','fix'=>$methods));
    }else{
      if(function_exists($method)){
        if($_REQUEST['help']=='true'){
            api_framework_output(200, $methods[$method]);
        }else{
          foreach($methods[$method]['inputs'] as $current){
            if($current['required']==true){
              if(api_framework_isblank($_REQUEST[$current['name']])){
                $error[] = 'Pass `'.$current['name'].'` via POST or GET';
              }
            }
          }
          if(api_framework_isblank($error)){
            $output = $method($_REQUEST);
            if(!api_framework_isblank($output['CODE'])){
              $code = $output['CODE'];
              unset($output['CODE']);
            }else{
              $code = 200; 
            }
            api_framework_output($code, $output);
          }else{
            api_framework_output(400, $error);
          }
        }
      }else{
        api_framework_output(404, array('error'=>'Method does not exist','fix'=>$methods));
      }
    }
  } 
  /* Framework Functions */
  function api_framework_output($code, $output){
    global $format;
    global $callback;
    header('Access-Control-Allow-Origin: *');
    if($callback=='false'){
      http_response_code(200);
      if($format=='print'){
        echo print_r($output,true);
      }elseif($format=='xml'){
        echo api_framework_xml_encode($output);
      }else{
        echo json_encode($output);
      }
    }elseif($callback==null){
      http_response_code($code);
      if($format=='print'){
        header('Content-Type: text/plain');
        echo print_r($output,true);
      }elseif($format=='xml'){
        header('Content-Type: application/xml');
        echo api_framework_xml_encode($output);
      }else{
        header('Content-Type: application/json');
        echo json_encode($output,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
      }
    }else{
      http_response_code(302);
      if($format=='print'){
        $data = print_r($output, true);
      }elseif($format=='xml'){
        $data = api_framework_xml_encode($output);
      }else{
        $data = json_encode($output);
      }
      header("Location: ".$callback."?callback=".base64_encode($data));
    }
  }
  function api_framework_isblank($isBlank_input = null){
    if (!isset($isBlank_input) || empty($isBlank_input) || is_null($isBlank_input) || $isBlank_input=="" || $isBlank_input==" "){
      return true;
    } else {
      return false;
    }
  }
  function api_framework_xml_encode($mixed, $domElement=null, $DOMDocument=null) {
    if (is_null($DOMDocument)) {
      $DOMDocument =new DOMDocument;
      $DOMDocument->formatOutput = true;
      api_framework_xml_encode($mixed, $DOMDocument, $DOMDocument);
      return $DOMDocument->saveXML();
    }
    else {
      if (is_array($mixed)) {
        foreach ($mixed as $index => $mixedElement) {
          if (is_int($index)) {
            if ($index === 0) {
              $node = $domElement;
            }
            else {
              $node = $DOMDocument->createElement($domElement->tagName);
              $domElement->parentNode->appendChild($node);
            }
          }
          else {
            $plural = $DOMDocument->createElement($index);
            $domElement->appendChild($plural);
            $node = $plural;
          }
          api_framework_xml_encode($mixedElement, $node, $DOMDocument);
        }
      }
      else {
        $mixed = is_bool($mixed) ? ($mixed ? 'true' : 'false') : $mixed;
        $domElement->appendChild($DOMDocument->createTextNode($mixed));
      }
    }
  }
  ?>